/* Copyright (c) 2017 Fabio Madeira
 *
 *  JAva Bioinformatics Analysis Web Services (JABAWS) @version: 2.0
 *
 *  This library is free software; you can redistribute it and/or modify it under the terms of the
 *  Apache License version 2 as published by the Apache Software Foundation
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 *  even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Apache
 *  License for more details.
 *
 *  A copy of the license is in apache_license.txt. It is also available here:
 * @see: http://www.apache.org/licenses/LICENSE-2.0.txt
 *
 * Any republication or derived work distributed in source code form
 * must include this copyright and license notice.
 */

package compbio.stat.servlet;

import compbio.stat.servlet.util.Totals;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

public class AnnualStatRefresher extends AnnualStat {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        Map<Date, Totals> monthlyTotals;
        String timeStamp = new String();
        long startTime;
        long endTime;

        // get stats from db
        startTime = System.nanoTime();
        monthlyTotals = checkMonthlyTotals(resp);
        endTime = System.nanoTime();
        if (monthlyTotals != null) {
            // try clear the previous values if any
            clearContextCache();
            // add the current values to the context
            this.getServletConfig().getServletContext().setAttribute("usageStatsResults", monthlyTotals);
            timeStamp = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(
                    Calendar.getInstance().getTime());
            this.getServletConfig().getServletContext().setAttribute("usageStatsTimestamp", timeStamp);
            this.getServletConfig().getServletContext().setAttribute("usageStatsStart", startTime);
            this.getServletConfig().getServletContext().setAttribute("usageStatsEnd", endTime);
        } else {
            return;
        }

        resp.sendRedirect("PublicAnnualStat");

    }

}