package compbio.stat.servlet.util;


import java.util.Calendar;
import java.util.Date;

/**
 * A <code>RefreshIterator</code> returns a sequence of dates on subsequent days
 * representing the same time each day.
 */
public class RefreshIterator implements ScheduleIterator {
    private final int hourOfDay, minute, second, freq_time;
    private final Calendar calendar = Calendar.getInstance();

    public RefreshIterator(int hourOfDay, int minute, int second, int freq_time) {
        this(hourOfDay, minute, second, freq_time, new Date());
    }

    public RefreshIterator(int hourOfDay, int minute, int second, int freq_time, Date date) {
        this.hourOfDay = hourOfDay;
        this.minute = minute;
        this.second = second;
        this.freq_time = freq_time;
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, second);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    public Date next() {
        calendar.add(Calendar.MINUTE, this.freq_time);
        return calendar.getTime();
    }

}
