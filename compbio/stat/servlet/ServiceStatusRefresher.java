/* Copyright (c) 2017 Fabio Madeira
 *
 *  JAva Bioinformatics Analysis Web Services (JABAWS) @version: 2.0
 *
 *  This library is free software; you can redistribute it and/or modify it under the terms of the
 *  Apache License version 2 as published by the Apache Software Foundation
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 *  even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Apache
 *  License for more details.
 *
 *  A copy of the license is in apache_license.txt. It is also available here:
 * @see: http://www.apache.org/licenses/LICENSE-2.0.txt
 *
 * Any republication or derived work distributed in source code form
 * must include this copyright and license notice.
 */

package compbio.stat.servlet;

import java.io.IOException;
import java.util.*;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ServiceStatusRefresher extends ServiceStatus {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        /**
         * // PROBLEM: the code tries to test not WS endpoints on an internal
         * Tomcat, but on an external // endpoints with wrong info on the
         * proxing StringBuffer jabawspath = req.getRequestURL(); jabawspath =
         * jabawspath.delete(jabawspath.lastIndexOf("/"), jabawspath.length());
         * String serverPath = jabawspath.toString();
         */

        ArrayList<ServiceTestResult> testResults = new ArrayList<ServiceTestResult>();
        String timeStamp = new String();
        long startTime;
        long endTime;
        String goodStatus;

        // test the services and timeit
        startTime = System.nanoTime();
        testResults = testServiceStatus(req);
        endTime = System.nanoTime();
        // try clear the previous values if any
        clearContextCache();
        // add the current values to the context
        this.getServletConfig().getServletContext().setAttribute("serviceStatusResults", testResults);
        timeStamp = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(
                Calendar.getInstance().getTime());
        this.getServletConfig().getServletContext().setAttribute("serviceStatusTimestamp", timeStamp);
        this.getServletConfig().getServletContext().setAttribute("serviceStatusStart", startTime);
        this.getServletConfig().getServletContext().setAttribute("serviceStatusEnd", endTime);
        goodStatus = overallStatusGood(testResults);
        this.getServletConfig().getServletContext().setAttribute("serviceStatusAllGood", goodStatus);

        resp.sendRedirect("ServiceStatus");

    }

}
