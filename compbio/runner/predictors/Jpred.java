/* Copyright (c) 2013 Alexander Sherstnev
 *  
 *  JAva Bioinformatics Analysis Web Services (JABAWS) @version: 3.0     
 * 
 *  This library is free software; you can redistribute it and/or modify it under the terms of the
 *  Apache License version 2 as published by the Apache Software Foundation
 * 
 *  This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 *  even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Apache 
 *  License for more details.
 * 
 *  A copy of the license is in apache_license.txt. It is also available here:
 * @see: http://www.apache.org/licenses/LICENSE-2.0.txt
 * 
 * Any republication or derived work distributed in source code form
 * must include this copyright and license notice.
 */

//package compbio.runner.predictors;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.IOException;
//import java.io.InputStream;
//import java.util.Arrays;
//import java.util.List;
//
//import org.apache.log4j.Logger;
//
//import compbio.data.sequence.JpredAlignment;
//import compbio.data.sequence.AlignmentMetadata;
//import compbio.data.sequence.Program;
//import compbio.data.sequence.SequenceUtil;
//import compbio.engine.client.CommandBuilder;
//import compbio.engine.client.Executable;
//import compbio.engine.client.SkeletalExecutable;
//import compbio.metadata.ResultNotAvailableException;
//
///**
// * Command line
// *
// * jpred.pl -in d16vpa_.fas -outfile res_d16vpa_ -dbname ported_db -dbpath
// * /data/UNIREFdb -ncpu 4
// *
// * @author asherstnev
// *
// */
//public class Jpred extends SkeletalExecutable<Jpred> {
//
//	private static Logger log = Logger.getLogger(Jpred.class);
//
//	/**
//	 * Number of cores to use, defaults to 1 for local execution or the value of
//	 * "jpred.cluster.cpunum" property for cluster execution
//	 */
//	private int ncoreNumber = 0;
//
//	public static final String KEY_VALUE_SEPARATOR = " ";
//	public static final String STAT_FILE = "stat.txt";
//
//	public Jpred() {
//		String dbpath = ph.getProperty("jpred.data.uniref.path");
//		String dbname = ph.getProperty("jpred.data.uniref.name");
//		addParameters(Arrays.asList("-logfile " + STAT_FILE));
//		addParameters(Arrays.asList("-dbpath " + dbpath));
//		addParameters(Arrays.asList("-dbname " + dbname));
//		addParameters(Arrays.asList("-jabaws"));
//	}
//
//	// HashMap<Method, float[]>
//	@SuppressWarnings("unchecked")
//	@Override
//	public JpredAlignment getResults(String workDirectory) throws ResultNotAvailableException {
//		JpredAlignment annotations = null;
//		try {
//			InputStream inStream = new FileInputStream(new File(workDirectory, getOutput()));
//			annotations = new JpredAlignment(SequenceUtil.readJpredFile(inStream), new AlignmentMetadata(Program.Jpred, '-'));
//		} catch (FileNotFoundException e) {
//			log.error(e.getMessage(), e.getCause());
//			throw new ResultNotAvailableException(e);
//		} catch (IOException e) {
//			log.error(e.getMessage(), e.getCause());
//			throw new ResultNotAvailableException(e);
//		} catch (NullPointerException e) {
//			log.error(e.getMessage(), e.getCause());
//			throw new ResultNotAvailableException(e);
//		}
//		return annotations;
//	}
//
//	@Override
//	public List<String> getCreatedFiles() {
//		return Arrays.asList(getOutput(), getError());
//	}
//
//	@Override
//	public Jpred setInput(String inFile) {
//		super.setInput(inFile);
//		cbuilder.setParam("-in " + inFile);
//		return this;
//	}
//
//	@Override
//	public Jpred setOutput(String outFile) {
//		super.setOutput(outFile);
//		cbuilder.setParam("-outfile " + outFile);
//		return this;
//	}
//
//	@SuppressWarnings("unchecked")
//	@Override
//	public Class<Jpred> getType() {
//		return (Class<Jpred>) this.getClass();
//	}
//
//	public static String getStatFile() {
//		return STAT_FILE;
//	}
//
//	public void setNCore(int ncoreNumber) {
//		if (0 < ncoreNumber && ncoreNumber < 9) {
//			this.ncoreNumber = ncoreNumber;
//			cbuilder.setParam("-ncpu " + Integer.toString(getNCore()));
//		} else {
//			throw new IndexOutOfBoundsException("Number of cores must be between 1 and 8 ");
//		}
//	}
//
//	int getNCore() {
//		return ncoreNumber;
//	}
//
//	@Override
//	public CommandBuilder<Jpred> getParameters(ExecProvider provider) {
//		// If number of cores is provided, set it for the cluster execution
//		// only!
//		if (provider == Executable.ExecProvider.Cluster) {
//			int cpunum = SkeletalExecutable.getClusterCpuNum(getType());
//			cpunum = (cpunum == 0) ? 1 : cpunum;
//			setNCore(cpunum);
//		} else {
//			// Limit number of cores to 1 for ANY execution which does not set
//			// Ncores explicitly using setNCore method or is run on local VM
//			if (ncoreNumber == 0) {
//				setNCore(1);
//			}
//		}
//		return super.getParameters(provider);
//	}
//
//}
