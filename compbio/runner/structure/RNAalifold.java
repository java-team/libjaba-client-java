package compbio.runner.structure;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.log4j.Logger;

import compbio.data.sequence.RNAStructScoreManager;
import compbio.engine.client.PipedExecutable;
import compbio.engine.client.SkeletalExecutable;
import compbio.metadata.ResultNotAvailableException;
import compbio.runner.RunnerUtil;

public class RNAalifold extends SkeletalExecutable<RNAalifold> 
		implements PipedExecutable<RNAalifold> {

	private static Logger log = Logger.getLogger(RNAalifold.class);

	// May not be necessary as defult is "<space>" but still dont know
	// How to deal with different key value separators for different params
	public static final String KEY_VALUE_SEPARATOR = " ";
	
	public RNAalifold() {
		super(KEY_VALUE_SEPARATOR);
	}
	
	@Override
	public RNAalifold setOutput(String outFile) {
		super.setOutput(outFile);
		return this;
	}
	
	@Override
	public RNAalifold setInput(String inFile) {
		cbuilder.setLast(inFile);
		super.setInput(inFile);
		return this;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Class<RNAalifold> getType() {
		return (Class<RNAalifold>) this.getClass();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public RNAStructScoreManager getResults(String workDirectory)
			throws ResultNotAvailableException {
		try {
			return RunnerUtil.readRNAStruct(workDirectory, getOutput());
		} catch (FileNotFoundException e) {
			log.error(e.getMessage(), e.getCause());
			throw new ResultNotAvailableException(e);
		} catch (IOException e) {
			log.error(e.getMessage(), e.getCause());
			throw new ResultNotAvailableException(e);
		}
	}

}
