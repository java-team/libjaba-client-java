package compbio.ws.server;

import java.util.List;

import javax.jws.WebService;

import org.apache.log4j.Logger;

import compbio.data.msa.JABAService;
import compbio.data.msa.SequenceAnnotation;
import compbio.data.sequence.FastaSequence;
import compbio.engine.client.ConfiguredExecutable;
import compbio.metadata.JobSubmissionException;
import compbio.metadata.LimitExceededException;
import compbio.metadata.Option;
import compbio.metadata.Preset;
import compbio.metadata.UnsupportedRuntimeException;
import compbio.metadata.WrongParameterException;
import compbio.runner.conservation.AACon;
import compbio.runner.structure.RNAalifold;

@WebService(endpointInterface = "compbio.data.msa.SequenceAnnotation", targetNamespace = JABAService.V2_SERVICE_NAMESPACE, serviceName = "RNAalifoldWS")
public class RNAalifoldWS extends SequenceAnnotationService<RNAalifold> implements SequenceAnnotation<RNAalifold> {

	private static Logger log = Logger.getLogger(RNAalifoldWS.class);
	
	public RNAalifoldWS() {
		super (new RNAalifold(), log);
	}
	
	// Only purpose is to replace "analize" with "fold" method 
	// so that a clustal input file and not fasta will be generated
	@SuppressWarnings("unchecked")
	@Override
	public String analize(List<FastaSequence> sequences)
			throws UnsupportedRuntimeException, LimitExceededException,
			JobSubmissionException {
		WSUtil.validateFastaInput(sequences);
		ConfiguredExecutable<RNAalifoldWS> confRNAalifold = init(sequences);
		return WSUtil.fold(sequences, confRNAalifold, log, "analize", getLimit(""));
	}

	// Only purpose is to replace "analize" with "fold" method 
	// so that a clustal input file and not fasta will be generated
	@SuppressWarnings("unchecked")
	@Override
	public String customAnalize(List<FastaSequence> sequences,
			List<Option<RNAalifold>> options) throws UnsupportedRuntimeException,
			LimitExceededException, JobSubmissionException,
			WrongParameterException {
		WSUtil.validateFastaInput(sequences);
		ConfiguredExecutable<RNAalifold> confRNAalifold = init(sequences);
		List<String> params = WSUtil.getCommands(options, RNAalifold.KEY_VALUE_SEPARATOR);
		confRNAalifold.addParameters(params);
		return WSUtil.fold(sequences, confRNAalifold, log, "customAnalize", getLimit(""));
	}
	
	
	/*
	 * No presets are supported, thus the result of this call will be as simple
	 * call to analize without parameters
	 */
	@Override
	public String presetAnalize(List<FastaSequence> sequences,
			Preset<RNAalifold> preset) throws UnsupportedRuntimeException,
			LimitExceededException, JobSubmissionException,
			WrongParameterException {
		return analize(sequences);
	}
}
