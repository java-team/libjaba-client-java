/* Copyright (c) 2011 Peter Troshin
 *  
 *  JAva Bioinformatics Analysis Web Services (JABAWS) @version: 2.0     
 * 
 *  This library is free software; you can redistribute it and/or modify it under the terms of the
 *  Apache License version 2 as published by the Apache Software Foundation
 * 
 *  This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 *  even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Apache 
 *  License for more details.
 * 
 *  A copy of the license is in apache_license.txt. It is also available here:
 * @see: http://www.apache.org/licenses/LICENSE-2.0.txt
 * 
 * Any republication or derived work distributed in source code form
 * must include this copyright and license notice.
 */

//package compbio.ws.server;
//
//import java.io.File;
//import java.util.List;
//
//import javax.jws.WebService;
//
//import org.apache.log4j.Logger;
//
//import compbio.data.msa.JABAService;
//import compbio.data.msa.MsaWS;
//import compbio.data.sequence.Alignment;
//import compbio.data.sequence.JpredAlignment;
//import compbio.data.sequence.FastaSequence;
//import compbio.engine.AsyncExecutor;
//import compbio.engine.Configurator;
//import compbio.engine.client.ConfiguredExecutable;
//import compbio.engine.client.Executable;
//import compbio.engine.client.SkeletalExecutable;
//import compbio.engine.client.EngineUtil;
//import compbio.metadata.ChunkHolder;
//import compbio.metadata.JobStatus;
//import compbio.metadata.JobSubmissionException;
//import compbio.metadata.Limit;
//import compbio.metadata.LimitsManager;
//import compbio.metadata.Option;
//import compbio.metadata.Preset;
//import compbio.metadata.PresetManager;
//import compbio.metadata.ResultNotAvailableException;
//import compbio.metadata.RunnerConfig;
//import compbio.metadata.WrongParameterException;
//import compbio.runner.RunnerUtil;
//import compbio.runner.predictors.Jpred;
//
//@WebService(endpointInterface = "compbio.data.msa.MsaWS", targetNamespace = JABAService.V2_SERVICE_NAMESPACE, serviceName = "JpredWS")
//public class JpredWS implements MsaWS<Jpred> {
//
//	private static Logger log = Logger.getLogger(JpredWS.class);
//
//	private static final RunnerConfig<Jpred> jpredOptions = RunnerUtil.getSupportedOptions(Jpred.class);
//	private static final PresetManager<Jpred> jpredPresets = RunnerUtil.getPresets(Jpred.class);
//	private static final LimitsManager<Jpred> limitMan = EngineUtil.getLimits(new Jpred().getType());
//
//	@Override
//	public String align(List<FastaSequence> sequences)
//			throws JobSubmissionException {
//		WSUtil.validateFastaInput(sequences);
//		ConfiguredExecutable<Jpred> confClust = init(sequences);
//		return WSUtil.align(sequences, confClust, log, "align", getLimit(""));
//	}
//
//	ConfiguredExecutable<Jpred> init(List<FastaSequence> dataSet)
//			throws JobSubmissionException {
//		Jpred jpred = new Jpred();
//		jpred.setInput(SkeletalExecutable.INPUT);
//		jpred.setOutput(SkeletalExecutable.OUTPUT);
//		jpred.setError(SkeletalExecutable.ERROR);
//		ConfiguredExecutable<Jpred> confJpred = Configurator.configureExecutable(jpred, dataSet);
//		// Set the number of threads for the cluster execution from conf file
//		if (confJpred.getExecProvider() == Executable.ExecProvider.Cluster) {
//			int clusterCpuNum = SkeletalExecutable.getClusterCpuNum(jpred.getType());
//			if (clusterCpuNum != 0) {
//				jpred.setNCore(clusterCpuNum);
//			}
//		}
//		return confJpred;
//	}
//
//	@Override
//	public String presetAlign(List<FastaSequence> sequences,
//			Preset<Jpred> preset) throws JobSubmissionException,
//			WrongParameterException {
//		WSUtil.validateFastaInput(sequences);
//		if (preset == null) {
//			throw new WrongParameterException("Preset must be provided!");
//		}
//		Limit<Jpred> limit = getLimit(preset.getName());
//		ConfiguredExecutable<Jpred> confClust = init(sequences);
//		confClust.addParameters(preset.getOptions());
//		return WSUtil.align(sequences, confClust, log, "presetAlign", limit);
//	}
//
//	@Override
//	public String customAlign(List<FastaSequence> sequences,
//			List<Option<Jpred>> options) throws JobSubmissionException,
//			WrongParameterException {
//		WSUtil.validateFastaInput(sequences);
//		ConfiguredExecutable<Jpred> confClust = init(sequences);
//		List<String> params = WSUtil.getCommands(options, Jpred.KEY_VALUE_SEPARATOR);
//		confClust.addParameters(params);
//		log.info("Setting parameters: " + params);
//		return WSUtil.align(sequences, confClust, log, "customAlign", getLimit(""));
//	}
//
//	@Override
//	public RunnerConfig<Jpred> getRunnerOptions() {
//		return jpredOptions;
//	}
//
//	@SuppressWarnings("unchecked")
//	@Override
//	public Alignment getResult(String jobId) throws ResultNotAvailableException {
//		WSUtil.validateJobId(jobId);
//		AsyncExecutor asyncEngine = Configurator.getAsyncEngine(jobId);
//		ConfiguredExecutable<Jpred> jpred = (ConfiguredExecutable<Jpred>) asyncEngine.getResults(jobId);
//		return (Alignment)jpred.getResults();
//	}
//
//	@Override
//	public Limit<Jpred> getLimit(String presetName) {
//		if (limitMan == null) {
//			// No limit is configured
//			return null;
//		}
//		Limit<Jpred> limit = limitMan.getLimitByName(presetName);
//		return limit;
//	}
//
//	@Override
//	public LimitsManager<Jpred> getLimits() {
//		return limitMan;
//	}
//
//	@Override
//	public boolean cancelJob(String jobId) {
//		WSUtil.validateJobId(jobId);
//		boolean result = WSUtil.cancelJob(jobId);
//		return result;
//	}
//
//	@Override
//	public JobStatus getJobStatus(String jobId) {
//		WSUtil.validateJobId(jobId);
//		JobStatus status = WSUtil.getJobStatus(jobId);
//		return status;
//	}
//
//	@Override
//	public PresetManager<Jpred> getPresets() {
//		return jpredPresets;
//	}
//
//	@Override
//	public ChunkHolder pullExecStatistics(String jobId, long position) {
//		WSUtil.validateJobId(jobId);
//		String file = Configurator.getWorkDirectory(jobId) + File.separator + Jpred.getStatFile();
//		ChunkHolder cholder = WSUtil.pullFile(file, position);
//		return cholder;
//	}
//
//}
