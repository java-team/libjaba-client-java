/* Copyright (c) 2011 Peter Troshin
 *  
 *  JAva Bioinformatics Analysis Web Services (JABAWS) @version: 2.0     
 * 
 *  This library is free software; you can redistribute it and/or modify it under the terms of the
 *  Apache License version 2 as published by the Apache Software Foundation
 * 
 *  This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 *  even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Apache 
 *  License for more details.
 * 
 *  A copy of the license is in apache_license.txt. It is also available here:
 * @see: http://www.apache.org/licenses/LICENSE-2.0.txt
 * 
 * Any republication or derived work distributed in source code form
 * must include this copyright and license notice.
 */
package compbio.ws.client;

class Constraints {

	final static String pseparator = "=";

	// Parameters for required command line options
	final static String hostkey = "-h";
	final static String servicekey = "-s";

	final static String listServices = "-list";
	final static String listServicesOld = "-list_services";
	final static String testKey = "-test";

	// Actions
	final static String inputkey = "-i";

	final static String paramList = "-parameters";
	final static String presetList = "-presets";
	final static String limitList = "-limits";

	// Options
	final static String paramFile = "-f";
	final static String outputkey = "-o";
	final static String parameterkey = "-p";
	final static String presetkey = "-r";

	final static String help_text = "\r\n" + 
			"Usage: java -jar <JAR file name> -h=host_and_context <-s=serviceName> ACTION [OPTIONS] \r\n" + 
			"\r\n" + 
			"       -h=<host_context> - a full URL to the JABAWS web server including context path e.g. http://10.31.1.159:8080/ws\r\n" + 
			"       -s=<ServiceName>  - a name of servise (the full list can be obtain with the option -list)\r\n" + 
			"                           <serviceName> is required for all ACTIONS except -list\r\n\r\n" + 
			"ACTIONS: \r\n" + 
			"        -i=<inputFile>  - defines an input file. For most services this is a FASTA file with initial sequences\r\n" + 
			"        -list           - lists all available web services\r\n" + 
			"        -test           - tests functioning of <ServiceName> \r\n" + 
			"        -parameters     - lists parameters supported by <ServiceName>\r\n" + 
			"        -presets        - lists presets supported by <ServiceName>\r\n" + 
			"        -limits         - lists limits of <ServiceName>\r\n" + 
			"\r\n" + 
			"N.B. If an input file is specified with -i other actions are ignored!\r\n\r\n" + 
			"OPTIONS (used with the -i action only):\r\n" + 
			"        -r=<presetName>  - name of the preset to use\r\n" + 
			"        -o=<outputFile>  - file with output results\r\n" + 
			"        -f=<paramFile>   - file with the list of parameters to use.\r\n\r\n" + 
			"Please note that -r and -f options cannot be used together. \r\n" + 
			"Alignment/Prediction is done with either preset or a parameters from the file, but not both!\r\n\r\n" + 
			"EXAMPLES: \r\n" +
			"<THECLIENT> is java -jar <JAR file name>\r\n\r\n" +
			"1) List all available services on the host:\r\n" + 
			"   <THECLIENT> -h=http://www.compbio.dundee.ac.uk/jabaws -list\r\n" + 
			"\r\n" + 
			"2) Test Clustal web service:\r\n" + 
			"   <THECLIENT> -h=http://www.compbio.dundee.ac.uk/jabaws -s=ClustalWS -test \r\n" + 
			"\r\n" + 
			"3) Align sequence from file input.txt with Probcons. Record resulting alignment \r\n" + 
			"   into the output.txt:\r\n" + 
			"   <THECLIENT> -h=http://www.compbio.dundee.ac.uk/jabaws -s=ProbconsWS -i=input.txt -o=output.txt\r\n" + 
			"\r\n" + 
			"4) Calculate disorder with Disembl take input from input.txt with output results\r\n " +
			"   to be printed out to the standard output:\r\n" + 
			"    <THECLIENT> -h=http://www.compbio.dundee.ac.uk/jabaws -s=DisemblWS -i=input.txt \r\n" + 
			"\r\n" + 
			"5) List all parameters available for AAconWS service:\r\n" + 
			"   <THECLIENT> -h=http://www.compbio.dundee.ac.uk/jabaws -s=AAconWS -parameters\r\n" + 
			"\r\n" + 
			"6) Calculate conservation with AAConWS using LANDGRAF method, for Clustal alignment \r\n" + 
			"   from input.txt and report the scores to the standard output:\r\n" + 
			"   <THECLIENT> -h=http://www.compbio.dundee.ac.uk/jabaws -s=AAconWS -i=input.txt -f=prm.txt \r\n" + 
			"\r\n" + 
			"   Where the content of prm.txt file is -m=LANDGRAF\r\n" + 
			"N.B. The list of the supported parameters can be obtained as shown in the example 5.\r\n" + 
			"";

}
