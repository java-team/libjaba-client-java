/* Copyright (c) 2009 Peter Troshin
 * Copyright (c) 2013 Alexander Sherstnev
 *  
 *  JAva Bioinformatics Analysis Web Services (JABAWS) @version: 1.0
 * 
 *  This library is free software; you can redistribute it and/or modify it under the terms of the
 *  Apache License version 2 as published by the Apache Software Foundation
 * 
 *  This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 *  even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the Apache 
 *  License for more details.
 * 
 *  A copy of the license is in apache_license.txt. It is also available here:
 * @see: http://www.apache.org/licenses/LICENSE-2.0.txt
 * 
 * Any republication or derived work distributed in source code form
 * must include this copyright and license notice.
 */

//package compbio.data.sequence;
//
//import java.util.List;
//import java.util.ArrayList;
//
//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//
//import compbio.util.annotation.Immutable;
//import compbio.data.sequence.Alignment;
///**
// * Multiple sequence alignment and Jpred prediction.
// *
// * Does not give any guarantees on the content of individual FastaSequece
// * records. It does not guarantee neither the uniqueness of the names of
// * sequences nor it guarantees the uniqueness of the sequences.
// *
// * @see FastaSequence
// * @see AlignmentMetadata
// *
// * @author pvtroshin
// *
// * @version 1.0 September 2009
// *
// */
//@XmlAccessorType(XmlAccessType.FIELD)
//public class JpredAlignment extends Alignment{
//
//	private JpredAlignment() {
//		// This has to has a default constructor for JaxB
//	}
//
//	/**
//	 * @param sequences
//	 * @param program
//	 * @param gapchar
//	 */
//	public JpredAlignment(List<FastaSequence> sequences, Program program, char gapchar) {
//		this.sequences = sequences;
//		this.metadata = new AlignmentMetadata(Program.Jpred, gapchar);
//	}
//
//	/**
//	 *
//	 * @param sequences
//	 * @param metadata
//	 */
//	public JpredAlignment(List<FastaSequence> sequences, AlignmentMetadata metadata) {
//		this.sequences = sequences;
//		this.metadata = metadata;
//	}
//
//	private List<FastaSequence> getRealSeqs() {
//		List<FastaSequence> realsequences = new ArrayList<FastaSequence>();
//		for (FastaSequence s : this.sequences) {
//			if (s.getId().equals("jnetpred") ||
//				s.getId().equals("JNETCONF") ||
//				s.getId().equals("JNETSOL25") ||
//				s.getId().equals("JNETSOL5") ||
//				s.getId().equals("JNETSOL0") ||
//				s.getId().equals("JNETHMM") ||
//				s.getId().equals("JNETHMM") ||
//				s.getId().equals("JNETSOL0") ||
//				s.getId().equals("JNETHMM")) {
//				// do nothing;
//			} else {
//				realsequences.add(s);
//			}
//		}
//		return realsequences;
//	}
//
//
//	public List<FastaSequence> getJpredSequences() {
//		List<FastaSequence> realsequences = new ArrayList<FastaSequence>();
//		for (FastaSequence s : this.sequences) {
//			if (s.getId().equals("jnetpred") ||
//				s.getId().equals("JNETCONF") ||
//				s.getId().equals("JNETSOL25") ||
//				s.getId().equals("JNETSOL5") ||
//				s.getId().equals("JNETSOL0") ||
//				s.getId().equals("JNETHMM") ||
//				s.getId().equals("JNETHMM") ||
//				s.getId().equals("JNETSOL0") ||
//				s.getId().equals("JNETSOL0") ||
//				s.getId().equals("QUERY")) {
//				realsequences.add(s);
//			}
//		}
//		return realsequences;
//	}
//	/**
//	 *
//	 * @return Jpred prediction
//	 */
//	public String getJpredPrediction() {
//		for (FastaSequence s : this.sequences) {
//			if (s.getId().equals("jnetpred")) {
//			  return s.getSequence();
//			}
//		}
//		return new String("");
//	}
//
//	/**
//	 *
//	 * @return Jpred JNETSOL25 line
//	 */
//	public String getJpredSOL25() {
//		for (FastaSequence s : this.sequences) {
//			if (s.getId().equals("JNETSOL25")) {
//			  return s.getSequence();
//			}
//		}
//		return new String("");
//	}
//
//	/**
//	 *
//	 * @return Jpred JNETSOL5 line
//	 */
//	public String getJpredSOL5() {
//		for (FastaSequence s : this.sequences) {
//			if (s.getId().equals("JNETSOL5")) {
//			  return s.getSequence();
//			}
//		}
//		return new String("");
//	}
//	/**
//	 *
//	 * @return Jpred JNETSOL0 line
//	 */
//	public String getJpredSOL0() {
//		for (FastaSequence s : this.sequences) {
//			if (s.getId().equals("JNETSOL0")) {
//			  return s.getSequence();
//			}
//		}
//		return new String("");
//	}
//	/**
//	 *
//	 * @return Jpred JNETCONF line
//	 */
//	public String getJpredJNETCONF() {
//		for (FastaSequence s : this.sequences) {
//			if (s.getId().equals("JNETCONF")) {
//			  return s.getSequence();
//			}
//		}
//		return new String("");
//	}
//
//	/**
//	 *
//	 * @return list of FastaSequence records
//	 */
//	@Override
//	public List<FastaSequence> getSequences() {
//		return this.getRealSeqs();
//	}
//
//	/**
//	 *
//	 * @return a number of sequence in the alignment
//	 */
//	@Override
//	public int getSize() {
//		return this.getRealSeqs().size();
//	}
//
//	/**
//	 * Please note that this implementation does not take the order of sequences
//	 * into account!
//	 */
//	@Override
//	public boolean equals(Object obj) {
//		if (obj == null) {
//			return false;
//		}
//		if (!(obj instanceof JpredAlignment)) {
//			return false;
//		}
//		JpredAlignment al = (JpredAlignment) obj;
//		if (this.getSize() != al.getSize()) {
//			return false;
//		}
//		if (!this.getMetadata().equals(al.getMetadata())) {
//			return false;
//		}
//		int outerCounter = 0;
//		int matchCounter = 0;
//		for (FastaSequence fs : getSequences()) {
//			outerCounter++;
//			for (FastaSequence fs1 : al.getSequences()) {
//				if (fs.equals(fs1)) {
//					matchCounter++;
//					continue;
//				}
//			}
//			// Match for at lease one element was not found!
//			if (outerCounter != matchCounter) {
//				return false;
//			}
//		}
//		return true;
//	}
//
//}
