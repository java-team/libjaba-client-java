
package compbio.data.msa.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "presetFold", namespace = "http://msa.data.compbio/01/01/2010/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "presetFold", namespace = "http://msa.data.compbio/01/01/2010/", propOrder = {
    "alignment",
    "preset"
})
public class PresetFold {

    @XmlElement(name = "alignment", namespace = "")
    private compbio.data.sequence.Alignment alignment;
    @XmlElement(name = "preset", namespace = "")
    private compbio.metadata.Preset preset;

    /**
     * 
     * @return
     *     returns Alignment
     */
    public compbio.data.sequence.Alignment getAlignment() {
        return this.alignment;
    }

    /**
     * 
     * @param alignment
     *     the value for the alignment property
     */
    public void setAlignment(compbio.data.sequence.Alignment alignment) {
        this.alignment = alignment;
    }

    /**
     * 
     * @return
     *     returns Preset
     */
    public compbio.metadata.Preset getPreset() {
        return this.preset;
    }

    /**
     * 
     * @param preset
     *     the value for the preset property
     */
    public void setPreset(compbio.metadata.Preset preset) {
        this.preset = preset;
    }

}
