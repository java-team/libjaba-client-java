
package compbio.data.msa.jaxws;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "customFold", namespace = "http://msa.data.compbio/01/01/2010/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "customFold", namespace = "http://msa.data.compbio/01/01/2010/", propOrder = {
    "alignment",
    "options"
})
public class CustomFold {

    @XmlElement(name = "alignment", namespace = "")
    private compbio.data.sequence.Alignment alignment;
    @XmlElement(name = "options", namespace = "")
    private List<compbio.metadata.Option> options;

    /**
     * 
     * @return
     *     returns Alignment
     */
    public compbio.data.sequence.Alignment getAlignment() {
        return this.alignment;
    }

    /**
     * 
     * @param alignment
     *     the value for the alignment property
     */
    public void setAlignment(compbio.data.sequence.Alignment alignment) {
        this.alignment = alignment;
    }

    /**
     * 
     * @return
     *     returns List<Option>
     */
    public List<compbio.metadata.Option> getOptions() {
        return this.options;
    }

    /**
     * 
     * @param options
     *     the value for the options property
     */
    public void setOptions(List<compbio.metadata.Option> options) {
        this.options = options;
    }

}
