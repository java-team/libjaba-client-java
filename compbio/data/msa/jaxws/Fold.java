
package compbio.data.msa.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "fold", namespace = "http://msa.data.compbio/01/01/2010/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fold", namespace = "http://msa.data.compbio/01/01/2010/")
public class Fold {

    @XmlElement(name = "alignment", namespace = "")
    private compbio.data.sequence.Alignment alignment;

    /**
     * 
     * @return
     *     returns Alignment
     */
    public compbio.data.sequence.Alignment getAlignment() {
        return this.alignment;
    }

    /**
     * 
     * @param alignment
     *     the value for the alignment property
     */
    public void setAlignment(compbio.data.sequence.Alignment alignment) {
        this.alignment = alignment;
    }

}
